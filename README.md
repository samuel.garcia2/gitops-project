# GITOPS AND CI/CD NGINX ON GKE



## Getting started

Project for testing Gitlab Gitops and pipeline/deployment nginx on Kubernetes GKE


```
cd existing_repo
git remote add origin https://gitlab.com/samuel.garcia2/gitops-project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/samuel.garcia2/gitops-project/-/settings/integrations)

## Authors and acknowledgment
samuel garcia - samuel.garcia@prodigio.tech

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
